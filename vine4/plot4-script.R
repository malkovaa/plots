library(ggplot2)
library(scales)
library(directlabels)

c <- read.csv("top4.csv")
c$time1 <- as.POSIXct(strptime(c$time1, "%Y-%m-%d %H:%M:%S"))
c$time2 <- as.POSIXct(strptime(c$time2, "%Y-%m-%d %H:%M:%S"))
c$time3 <- as.POSIXct(strptime(c$time3, "%Y-%m-%d %H:%M:%S"))
x1 <- c(c$time1)
y1 <- c(c$retweets)
k1 <- x1[1]
v1 <- y1[1]
x2 <- c(c$time2)
y2 <- c(c$dif_loops)
k2 <- x2[1]
v2 <- y2[2]
x3 <- c(c$time2)
y3 <- c(c$dif_revines)
k3 <- x3[1]
v3 <- y3[2]
x4 <- c(c$time2)
y4 <- c(c$dif_likes)
k4 <- x4[1]
v4 <- y4[2]
x5 <- c(c$time2)
y5 <- c(c$dif_comments)
k5 <- x5[1]
v5 <- y5[2]
x6 <- c(c$time3)
y6 <- c(c$tweet)
k6 <- x6[1]
v6 <- y6[1]
df1<-data.frame(x=c$time1,y=c$retweets)
df2<-data.frame(x=c$time2,y=c$dif_loops)
df3<-data.frame(x=c$time2,y=c$dif_revines)
df4<-data.frame(x=c$time2,y=c$dif_likes)
df5<-data.frame(x=c$time2,y=c$dif_comments)
df6<-data.frame(x=c$time3,y=c$tweet)


p <- ggplot(df1,aes(x,y))+geom_line(aes(color = "Retweets",group=1), stat="identity") + 
  geom_line(data=df2, aes(color="Loops", group=1), stat="identity") + 
  geom_line(data=df3, aes(color="Revines", group=1), stat="identity") + 
  geom_line(data=df4, aes(color="Likes", group=1), stat="identity") + 
  geom_line(data=df5, aes(color="Comments", group=1), stat="identity") + 
  geom_line(data=df6, aes(color="Tweets", group=1), stat="identity") + 
  theme(axis.text.x = element_text(angle = 45, hjust = 1), plot.title = element_text(hjust = 0.5),legend.title = element_blank()) + 
  coord_cartesian(ylim=c(1, 1500000)) +  
  scale_y_continuous(trans=log_trans(), breaks=c(1,10,100,10000,1000000), labels=comma)   



p + xlab("Time") + ylab("Number in a period of time") + 
  ggtitle("Top-20 Vines", subtitle = "Rank 4:  iiXhlAw0eJp")  +  
  scale_color_manual(values=c("#FFFF00", "#00CC99", "#CC00CC", "#3399FF","#FF9933", "#666600")) +  
  theme(plot.title = element_text(hjust = 0.5),plot.subtitle = element_text(hjust = 0.5),legend.title = element_blank()) + 
  scale_x_datetime(breaks = date_breaks("12 hour"),minor_breaks = date_breaks("12 hour")) + 
  geom_text(data = df1, aes(x = k1, y = v1, label = k1, angle = 90, hjust=0, vjust=0, color="Retweets")) + 
  geom_text(data = df2, aes(x = k2, y = v2, label = "created at: 2016-01-26 00:54:02", angle = 0, hjust=0.15, vjust=0.1, color = "Loops")) + 
  geom_text(data = df3, aes(x = k3, y = v3, label = k3, angle = 45, hjust=0, vjust= 1, color = "Revines")) + 
  geom_text(data = df6, aes(x = k6, y = v6, label = k6, angle = 45, hjust=0, vjust= 0, color = "Tweets")) 
#geom_text(data = df4, aes(x = k4, y = v4, label = k4, angle = 45, hjust=0, vjust= 0, color = "Likes")) + 
#geom_text(data = df5, aes(x = k5, y = v5, label = k5, angle = 0, hjust=0, vjust=1, color = "Comments"))
